import getDataAuth from '../utils/getDataAuth';
import { API_URL } from '../constants/index';
import { ASSET_DATA } from '../mock-data/assets-data';

 
function createArrayAssets(assets) {
    let arrAssets = [];
    assets.map(asset => arrAssets.push(fetchAssetsInfo(asset.id, asset.asset_type_id)))
    return arrAssets;
};

function createArrayAssetsSearch(assets) {
    let arrAssets = [];
    assets.map(asset => arrAssets.push(fetchAssetsInfoSearch(asset)))
    return arrAssets;
};

function fetchAssetsInfo(idAsset, assetType) {
    const data = {
        audience: 1,
        embargo: true,
        languages: [
            6,
            7
        ],
        description: [
            {
                '6': {
                    name: "iqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea c",
                    thumbnail_url: " exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Dui"
                }
            },
            {
                '7': {
                    name: "ae",
                    thumbnail_url: "velit esse cillum dolore eu fugiat nulla"
                }
            },
        ]
    }
    data.assetID = idAsset;
    data.assetType = assetType;
    return data;
}

function fetchAssetsInfoSearch(idAsset) {
    const data = {
        audience: 1,
        embargo: true,
        languages: [
            6,
            7
        ],
        description: [
            {
                '6': {
                    name: "asset search",
                    thumbnail_url: " exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Dui"
                }
            },
            {
                '7': {
                    name: "ae",
                    thumbnail_url: "velit esse cillum dolore eu fugiat nulla"
                }
            },
        ]
    }
  
    return data;
}

function createArrayCampaigns(campaigns) {
    let arrCampaigns = [];
    campaigns.map(campaign => arrCampaigns.push(fetchCampaignsInfo(campaign)))
    return arrCampaigns;
};

function createArrayCampaignsSearch(campaigns) {
    let arrCampaigns = [];
    campaigns.map(campaign => arrCampaigns.push(fetchCampaignsInfoSearch(campaign)))
    return arrCampaigns;
};

function fetchCampaignsInfo(idCampaign) {
    const data = {
        campaign_id: 2,
        audience: 1,
        featured: 0,
        embargo: true,
        languages: [
            2,
            1
        ],
        description: [
            {
                '2': {
                    name: "Mi campaña",
                    thumbnail_url: "http://fakeurl.com/mi-campana"
                }
            },
            {
                '1': {
                    name: "My Campaign",
                    thumbnail_url: "http://fakeurl.com/mi-campaign"
                }
            },
        ]
    }
    data.assetID = 24;
    return data;
       
}

function fetchCampaignsInfoSearch(idCampaign) {
    const data = {
        campaign_id: 2,
        audience: 1,
        featured: 0,
        embargo: true,
        languages: [
            2,
            1
        ],
        description: [
            {
                '2': {
                    name: "Mi campaña search",
                    thumbnail_url: "http://fakeurl.com/mi-campana"
                }
            },
            {
                '1': {
                    name: "My Campaign search",
                    thumbnail_url: "http://fakeurl.com/mi-campaign"
                }
            },
        ]
    }
    data.assetID = 24;
    return data;
       
}

function createArrayResources(resources) {
    let arrResources = [];
    resources.map(resource => arrResources.push(fetchResourcesInfo(resource)))
    return arrResources;
};

function fetchResourcesInfo(idResource) {
    const data =  {
        embargo: false,
        languages: [
            1
        ],
        description: [
            {
                '1': {
                    name: "My gateway resource updated",
                    thumbnail_url: "http://myurl-test.test"
                }
            }
        ]
    
    }

    return data;
}

export const fetchAuthLogin = (user, pass) => dispatch => {
    const dataResponse = {
        token_type: "Bearer",
        expires_in: 31622400,
        access_token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjkwNTQyZjA0NGE4NWE4YTM1MTE5ZjM5YTIxY2YyYTY4NmVhZDA0MGY4NWY0MzRkOWMxOGJlYzdlOTlkMTc4ODJlZDk5MmZjMzAyOWNkZGNjIn0.eyJhdWQiOiIyIiwianRpIjoiOTA1NDJmMDQ0YTg1YThhMzUxMTlmMzlhMjFjZjJhNjg2ZWFkMDQwZjg1ZjQzNGQ5YzE4YmVjN2U5OWQxNzg4MmVkOTkyZmMzMDI5Y2RkY2MiLCJpYXQiOjE1NTgzODQ1MjAsIm5iZiI6MTU1ODM4NDUyMCwiZXhwIjoxNTkwMDA2OTIwLCJzdWIiOiIxIiwic2NvcGVzIjpbIioiXX0.YD2lWTWBhz3e5-x8Sy8hCurVZojmFPiuWRCKQ1KhI_qj69pCMT89PGHg9ssobgY_LgjCislBu0bSalC37oR_NSrIl6hOGZkiYvUCFhzyWyoZ2wOptmGDObbCFTSlICFyI19MgNvaqo-6nwGQnS4p-OXmgh9bsi7TqZL5sQvWDE7xoyuWE-Y-hVKfa3VTYhX0-MotCJm2WK5Tp7DoqARB1IrjJHnBPur-ld8tYcJ3RxH5VNMB58bRgIb-cdWXKVuET52H3G3heE_f06mjksvu3BlmWMXlB6D2r3sbeJ7Ws0W_aIOExi33Fk4z6VklxsM9rlF8pKigxiQLuKzAe1mb0YwbSvOyEsYDoch587biJMLr_xQoEZrRRnH1_yYpyHlo_TwC5BdVLDrL9AkjQNrWrxVj7asX1VKDkKIs1tzjCcWzhOH-TlDFmGKJwaN5GdZlCIbZGo7Fxv-bmwUkwQrG9-yA1XknD_tZN4nA1dnD92B79govFO_caxJ9Fa3IlSivGKe5HKMWtvy-gQRspatIHPGPJbst9PWorUcK2YIQjhnRVivWRwY2bHd_wNUulhoQDg3Rx1QrOimgJBkVD1BTlyp2dgrBgDgaKbKhv8eC5XfEK76vBkqWkrO7FwH8C8kJxtHDiTmUUqA-mJVl1jcHbwK93TZhxtKXHKmS1YCE8Lc",
        refresh_token: "def50200d24975f61da53f418112fafadfe734d15533eb518ae0ab408c9a4ad16264e46db63fad9c04e8d9f7e25ea665870f272c16a559a6d8b3248a0620f357d563c7655526503b14aacc1652faf39b97942336652dfcc5e3b1c91d4dff23a13261e9ef61359e9b94700bfa77c089419694721ec0d375ca7a3a3f4254d49bda31b7241e5ce1f74ddb4e939b382663e4a1483fd78a54ce0ba7c295d19d3742aa997c59fc42ed1f19185bbdb66956eae0ead9dee55b7fe62a0cc74c871fb40c742d60633ba1d2d05edb26b30da06f7314f353453b6db1bfd8dd975a192bac75db8901a257324a4d21923048467854158ba98241855c6c753699c36d017b2f01fc960be4e060c8a618ca439e66926964d30b7f195615a02bdbb76dbf07c8d658d0a361abe593af590a42895d68391737cc8e8558e4627f4f2499b90b9b953201a67c982e785de5b1a171cf4a55c2b2ab4445019c9878a6deac4f7297bf3541d9745410f96b"
    }

    localStorage.setItem('auth', JSON.stringify(dataResponse));
    dispatch(authLoginSuccess(dataResponse));

    /*const dataLogin = {
        grant_type: 'password',
        client_id: "2",
        client_secret: "E8WPAPuALsU2dQAxAIE8K2RhO5l9IqH00LGrwjUi",
        username: user,
        password: "pass",
        scope: "*"
    };
    fetch(`${API_URL}/oauth/token`, {
        method: 'post',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(dataLogin)
    })
    .then(response => response.json())
    .then(jsondata => console.log(jsondata))
    .catch (error => {
        console.log('Request failed', error);
    }); */

};

export const fetchPreferencesUser = () => dispatch => {

    const dataResponse = {
        idUser: 2,
        filterPage: 20,
    }
    dispatch(fetchPreferencesUserSuccess(dataResponse));

    /*const dataLogin = {
        grant_type: 'password',
        client_id: "2",
        client_secret: "E8WPAPuALsU2dQAxAIE8K2RhO5l9IqH00LGrwjUi",
        username: user,
        password: "pass",
        scope: "*"
    };
    fetch(`${API_URL}/oauth/token`, {
        method: 'post',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(dataLogin)
    })
    .then(response => response.json())
    .then(jsondata => console.log(jsondata))
    .catch (error => {
        console.log('Request failed', error);
    }); */

};

export const fetchPreferencesUserSuccess = (data) => ({
    type: 'FETCH_PREFERENCES_SUCCESS',
    data,
});


export const authLoginSuccess = (data) => ({
    type: 'FETCH_AUTH_SUCCESS',
    data,
});

export const fetchLogOut = () => dispatch => {
     const token = getDataAuth().access_token;
     fetch(`${API_URL}/oauth/logout`, {
         method: 'GET',
         headers: {
             'Accept': 'application/json',
             'Content-Type': 'application/json',
             'Authorization': `Bearer ${token}`
         },
     })
         .then(response => response.json())
         .then(jsondata => 
            dispatch(fetchLogOutSuccess(jsondata))
            )
         .catch(error => {
             alert('Error al realizar logout');
         });     
 };


export const fetchLogOutSuccess = () => ({
    type: 'LOG_OUT'
});

export const cleanListAssets = () => ({
    type: 'CLEAN_LIST_ASSETS'
});

export const cleanListCampaigns = () => ({
    type: 'CLEAN_LIST_CAMPAIGNS'
});

export const cleanListResources = () => ({
    type: 'CLEAN_LIST_RESOURCES'
});

export const cleanFilterApplied = () => ({
    type: 'CLEAN_FILTER_APPLIED'
});

export const fetchAssets = () => dispatch => {
   /* const token = getDataAuth().access_token;
    fetch(`${API_URL}/asset`, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': token
        },
    })
        .then(response => response.json())
        .then(jsondata => console.log(jsondata))
        .catch(error => {
            console.log('Request failed', error);
        });*/

        
         
    const data = ASSET_DATA;   
    //const data = [687, 342, 934, 600, 346, 327];
    dispatch(fetchAssetsInfoSuccess(createArrayAssets(data)));
};


export const fetchAssetsInfoSuccess = (data) => ({
    type: 'FETCH_ASSET_INFO_SUCCESS',
    data,
});

export const fetchCampaigns = () => dispatch => {
    /* const token = getDataAuth().access_token;
     fetch(`${API_URL}/asset`, {
         method: 'GET',
         headers: {
             'Accept': 'application/json',
             'Content-Type': 'application/json',
             'Authorization': token
         },
     })
         .then(response => response.json())
         .then(jsondata => console.log(jsondata))
         .catch(error => {
             console.log('Request failed', error);
         });*/
     //const data = [687, 342, 934, 600, 346, 327];
     const data = [
        {
            id: 1,
            upload_date: "2011-02-24 10:05:09",
            asset_type_id: 1,
            embargo: "2019-12-30",
            featured: 0,
            expiration: "2019-12-28",
            active: 1,
            created_at: "1993-04-08 18:49:17",
            updated_at: "2019-04-11 20:07:15"
        },
        {
            id: 3,
            upload_date: "2019-12-25 04:07:41",
            asset_type_id: 1,
            embargo: "2019-10-04",
            featured: 0,
            expiration: "2019-12-21",
            active: 1,
            created_at: "2002-09-11 20:16:44",
            updated_at: "1988-09-11 21:09:08"
        },
        {
            id: 4,
            upload_date: "2019-05-07 14:27:44",
            asset_type_id: 1,
            embargo: "2019-12-30",
            featured: 0,
            expiration: "2019-12-28",
            active: 1,
            created_at: "2019-05-07 14:27:44",
            updated_at: "2019-05-07 14:27:44"
        }
    ]
     dispatch(fetchCampaignsInfoSuccess(createArrayCampaigns(data)));
 };
 
 
 export const fetchCampaignsInfoSuccess = (data) => ({
     type: 'FETCH_CAMPAIGNS_INFO_SUCCESS',
     data,
 });

 export const fetchResources = () => dispatch => {
    /* const token = getDataAuth().access_token;
     fetch(`${API_URL}/asset`, {
         method: 'GET',
         headers: {
             'Accept': 'application/json',
             'Content-Type': 'application/json',
             'Authorization': token
         },
     })
         .then(response => response.json())
         .then(jsondata => console.log(jsondata))
         .catch(error => {
             console.log('Request failed', error);
         });*/
     const data = [687];
     dispatch(fetchResourcesInfoSuccess(createArrayResources(data)));
 };
 
 
 export const fetchResourcesInfoSuccess = (data) => ({
     type: 'FETCH_RESOURCES_INFO_SUCCESS',
     data,
 });
 
export const fetchSearch = (strSearch, name) => dispatch =>{
    const data= {
        assets: [
            687,
            342,
        ],
        campaigns: [
            687,
        ],
        resources: [
            687,
            934,
            342,
            600,
            346
        ]
    }

    if(data['assets']){
        dispatch(fetchAssetsInfoSuccess(createArrayAssetsSearch(data['assets'])));
    }
    else{
        dispatch(fetchAssetsInfoSuccess(null));
    }

    if(data['campaigns']){
        dispatch(fetchCampaignsInfoSuccess(createArrayCampaignsSearch(data['campaigns'])));
    }
    else{
        dispatch(fetchCampaignsInfoSuccess(null));
    }

    //dispatch(fetchCampaignsInfoSuccess(createArrayCampaigns(data)));
};

export const fetchFilter = () => dispatch => {
    // const token = getDataAuth().access_token;
    // console.log(token);
    // fetch(`${API_URL}/filter`, {
    //     method: 'GET',
    //     headers: {
    //         'Accept': 'application/json',
    //         'Content-Type': 'application/json',
    //         'Authorization': token
    //     },
    // })
    //     .then(response => response.json())
    //     .then(jsondata => console.log(jsondata))
    //     .catch(error => {
    //         console.log('Request failed', error);
    //     });

    const data = [
        {
            id: 1,
            user_id: 6,
            name: "Filter new",
            value: "filter filter",
            created_at: "2019-04-24 19:42:41",
            updated_at: "2019-05-03 15:14:59"
        },
        {
            id: 2,
            user_id: 6,
            name: "Filter 2",
            value: "filter2",
            created_at: "2019-04-24 19:47:26",
            updated_at: "2019-04-24 19:47:26"
        },
        {
            id: 5,
            user_id: 8,
            name: "Filter Filter",
            value: "filter filter",
            created_at: "2019-04-24 20:35:57",
            updated_at: "2019-04-24 20:35:57"
        },
        {
            id: 6,
            user_id: 6,
            name: "Filter 3",
            value: "filter3",
            created_at: "2019-05-03 15:14:58",
            updated_at: "2019-05-03 15:14:58"
        },
        {
            id: 7,
            user_id: 6,
            name: "Filter 3",
            value: "filter3",
            created_at: "2019-05-03 15:15:19",
            updated_at: "2019-05-03 15:15:19"
        },
        {
            id: 8,
            user_id: 8,
            name: "Filter Filter",
            value: "filter filter",
            created_at: "2019-05-09 19:30:19",
            updated_at: "2019-05-09 19:30:19"
        }
    ]
    dispatch(fetchFiltersSuccess(data));
}

export const fetchFiltersSuccess = (data) => ({
    type: 'FETCH_FILTERS_SUCCESS',
    data,
});

export const addFilter = (data) => ({
    type: 'ADD_FILTER',
    data,
})

export const removeFilter = (data) => ({
    type: 'REMOVE_FILTER',
    data,
})

export const fetchAssetsFilters = (filters) => dispatch => {
    console.log('filtros aplicados');
    console.log(filters);
    // const token = getDataAuth().access_token;
    // console.log(token);
    // fetch(`${API_URL}/getFilters`, { //TODO 
    //     method: 'GET',
    //     headers: {
    //         'Accept': 'application/json',
    //         'Content-Type': 'application/json',
    //         'Authorization': token
    //     },
    // })
    //     .then(response => response.json())
    //     .then(jsondata => console.log(jsondata))
    //     .catch(error => {
    //         console.log('Request failed', error);
    //     });
    const data = [327, 327];
    dispatch(fetchAssetsInfoSuccess(createArrayAssets(data)));
}

export const fetchAssetsFiltersSuccess = (data) => ({
    type: 'FETCH_ASSETS_FILTER',
    data,
})

export const fetchFilterPages = (pages) => dispatch => {
    //  const token = getDataAuth().access_token;
    //  fetch(`${API_URL}/user/config/1?pagination=${pages}`, {
    //          method: 'PUT',
    //          headers: {
    //              'Accept': 'application/json',
    //              'Content-Type': 'application/json',
    //              'Authorization': token
    //          },
    //      })
    //      .then(response => response.json())
    //      .then(jsondata => dispatch(fetchFilterPagesSuccess(pages)))
    //      .catch(error => {
    //          alert('Error al ejecutar el servicio');
    //      });
    
    dispatch(fetchFilterPagesSuccess(pages))
}

export const fetchFilterPagesSuccess = (data) => ({
    type: 'FETCH_FILTER_PAGES',
    data,
})

export const fetchFilterData = (filter) => dispatch => {
    /*const token = getDataAuth().access_token;
    //  fetch(`${API_URL}/user/config/1?sort_by=${filter}`, {
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': token
        },
    })
        .then(response => response.json())
        .then(jsondata => dispatch(fetchFilterDataSuccess(filter)))
        .catch(error => {
            alert('Error al ejecutar el servicio');
        });*/
        dispatch(fetchFilterDataSuccess(filter));
}

export const fetchFilterDataSuccess = (data) => ({
    type: 'FETCH_FILTER_DATA',
    data,
})