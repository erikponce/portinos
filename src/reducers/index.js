const initialState = {filterApplied:[]}
const reducer = (state = initialState, action) => {
    const newState = { ...state };
    switch(action.type) {
        case 'FETCH_AUTH_SUCCESS':
            newState.auth = action.data;
            return newState;
        case 'FETCH_PREFERENCES_SUCCESS':
            newState.preferences = action.data;
            return newState;
        case 'LOG_OUT':
            newState.auth = null;
            return newState;
        case 'FETCH_ASSET_INFO_SUCCESS':
            newState.assets = action.data;
            return newState;
        case 'FETCH_CAMPAIGNS_INFO_SUCCESS':
            newState.campaigns = action.data;
            return newState;
        case 'FETCH_RESOURCES_INFO_SUCCESS':
            newState.resources = action.data;
            return newState;
        case 'CLEAN_LIST_ASSETS':
            newState.assets = null;
            return newState;
        case 'CLEAN_LIST_CAMPAIGNS':
            newState.campaigns = null;
            return newState;
        case 'CLEAN_LIST_RESOURCES':
            newState.resources = null;
            return newState;
        case 'CLEAN_FILTER_APPLIED':
            newState.filterApplied = [];
            return newState;
        case 'FETCH_FILTERS_SUCCESS':
            newState.filters = action.data;
            return newState;
        case 'ADD_FILTER':
            newState.filterApplied = newState.filterApplied.concat(action.data); 
            return newState;
        case 'REMOVE_FILTER':
            newState.filterApplied = newState.filterApplied.filter(filter => filter.name !== action.data.name); 
            return newState;
        case 'FETCH_ASSETS_FILTER':
            newState.assets = action.data; 
            return newState;
        case 'FETCH_FILTER_PAGES':
            newState.filterPages = action.data; 
            return newState;
        case 'FETCH_FILTER_DATA':
            newState.filterData = action.data; 
            return newState;
        default:
            return state;
        }
}
export default reducer;
