export default () => {
    const auth = localStorage.getItem('auth');
    let dataAuth;
    if (auth) {
        dataAuth = JSON.parse(auth);
    }
    else{
        dataAuth = '';
    }
    return dataAuth;
}