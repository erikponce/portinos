import React from "react";
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import NavBar from './components/NavBar/NavBar';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import HomePage from "./containers/HomePage/HomePage";
import Login from "./components/Login/Login";
import ListAssetPage from "./containers/ListAssetPage/ListAssetPage";
import ListCampaignPage from "./containers/ListCampaignPage/ListCampaignPage";
import ListResourcePage from "./containers/ListResourcePage/ListResourcePage";


function App() {
  return (
    <div>
      <Router>
        <Header />
        <NavBar />
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route path="/login" component={Login} />
          <Route path="/listAsset" component={ListAssetPage} />
          <Route path="/listCampaign" component={ListCampaignPage} />
          <Route path="/listResources" component={ListResourcePage} />
        </Switch>
        <Footer />
      </Router>
    </div>
  );
}

export default App;