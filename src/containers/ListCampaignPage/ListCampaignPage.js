import React, { Component } from 'react';
import Search from '../../components/Search/Search';
import Campaigns from '../../components/Campaigns/Campaigns';
import checkUserLoggedIn from '../../utils/checkUserLoggedIn';
import getDataAuth from '../../utils/getDataAuth';
import Filters from '../../components/Filters/Filters';
import { createBrowserHistory } from 'history';

const history = createBrowserHistory();


class ListCampaignPage extends Component {
  constructor(props) {
    super(props);
    this.state = {

    }

  }

  render() {
    //checkUserLoggedIn();
    if (getDataAuth() === '') {
      //window.location.href='/';

      this.props.history.push('/');
    }
    //checkUserLoggedIn();
    return (
      <div className="display">
        <Search />
        <div className="listing">
          <div className="listing-filters"> <Filters /> </div>
          <div className="listing-elements"> <Campaigns /> </div>
        </div>
      </div>
    )
  }
}

export default ListCampaignPage;
