import React, { Component } from 'react';
import checkUserLoggedIn from '../../utils/checkUserLoggedIn';
import getDataAuth from '../../utils/getDataAuth';
import Search from '../../components/Search/Search';
import Assets from '../../components/Assets/Assets';
import Campaigns from '../../components/Campaigns/Campaigns';
import Resources from '../../components/Resources/Resources';
import * as actions from '../../actions/fetchActions';
import { connect } from 'react-redux';

class HomePage extends Component {

  componentWillMount() {
      this.props.cleanListAssets();
      this.props.cleanListCampaigns();
      this.props.cleanListResources();
      this.props.cleanFilterApplied();
  }
    
  render() {
    if(getDataAuth() === ''){
      return (
        <div>
          <div>
            Welcome to the unified marketing repository
          </div>
          <div>
            Your one-stop shop for ready-to-use material and campaigns.
          </div>
        </div>
       )
    }
    return (
     <div>
        <div>
          <b>Bienvenido, usuario</b>
        </div>
        <div>
           <Search />
        </div>
        <div>
           <Assets />
           <Campaigns />
           <Resources />
        </div>
     </div>
    )
  }
}

export default connect(null,actions)(HomePage);
