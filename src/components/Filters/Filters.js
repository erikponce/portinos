import React, { Component } from 'react'
import { connect } from 'react-redux';
import * as actions from '../../actions/fetchActions';

class Filters extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filter: []
    }
  }


  componentDidMount() {
    this.props.fetchFilter();
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.filterApplied !== nextProps.filterApplied){
      this.props.fetchAssetsFilters(nextProps.filterApplied);
    }
  }

  changeFiltersApplied(filter, check){
    if(check.checked){
      this.props.addFilter(filter);
    }
    else{
      this.props.removeFilter(filter);
    }
  }

  render() {
    if (this.props.filters) {
      return (
        <div>
          <h3>Filters</h3>
          <table>
            {this.props.filters.map(item =>
              <tr>
                <td>
                  <label>{item.name}</label>
                </td>
                <td>
                  <input type='checkbox' name={item.id} value={item.id} onClick={(e)=>this.changeFiltersApplied(item, e.target)}></input>
                </td>
              </tr>)}
          </table>
        </div>
      ) 
    } else {
      return (
        <div>
          cargando filtros..
        </div>
      )
    }
  }
}

const mapStateToProps = (state) => {
  return {
    filters: state.filters,
    filterApplied: state.filterApplied
  }
}

export default connect(mapStateToProps, actions)(Filters);



// var Checkbox = React.createClass({
//     getInitialState: function() {
//       return {checked: false}
//     },
//     handleCheck: function() {
//       this.setState({checked: !this.state.checked});
//     },
//     render: function() {
//       var msg;
//       if (this.state.checked) {
//         msg = "checked";
//       } else {
//         msg = "unchecked";
//       }
//       return (
//         <div>
//           <input type="checkbox" onChange={this.handleCheck} defaultChecked={this.state.checked}/>
//           <p>this box is {msg}.</p>
//         </div>
//       );
//     }
//   });

//   React.render(<Checkbox />,
//     document.getElementById('react-container')
//   );