import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Link } from "react-router-dom";


class NavBar extends Component {
    
  render() {
    return (
     <div className="navbar">
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          
          <li>
            <Link to="/listAsset">Assets</Link>
          </li>
          <li>
            <Link to="/listCampaign">Campaigns</Link>
          </li>
          <li>
            <Link to="/listResources">Resources</Link>
          </li>
        </ul>

     </div>
    )
  }
}
export default NavBar;
