import React, { Component } from 'react'
import { connect } from 'react-redux';
import * as actions from '../../actions/fetchActions';
import CardAsset from './CardAsset/CardAsset';
import FilterApplied from '../FilterApplied/FilterApplied';


class Assets extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // asset: this.props.asset,
            displayedAssets: [],
            pageIndex: 0,
            resultsPerPage: 6
        }
    }


    componentDidMount() {
       // this.props.cleanFilterApplied();
        this.props.fetchAssets();
    }

    componentWillReceiveProps(nextProps) {
        
        if (nextProps.filterPages && nextProps.assets){
            let rpp = parseInt(nextProps.filterPages);
            this.setState({resultsPerPage: rpp});
            this.loadPaginator(nextProps.assets, rpp);
        } else if (nextProps.assets) {
            this.loadPaginator(nextProps.assets, this.state.resultsPerPage);
        }
        
    }

    loadPaginator(_assets, _resultsPerPage) {
        if (_resultsPerPage <= _assets.length) {
            let _displayedAssets = [..._assets].slice((this.state.pageIndex * _resultsPerPage), (this.state.pageIndex * _resultsPerPage) + _resultsPerPage);
            this.setState({displayedAssets: _displayedAssets});
        } else {
            this.setState({displayedAssets: _assets});
        }
    }

    reloadPaginator(pIndex) {
        this.setState({pageIndex : pIndex})
        let _displayedAssets = [...this.props.assets].slice((pIndex * this.state.resultsPerPage), (pIndex * this.state.resultsPerPage) + this.state.resultsPerPage);
        this.setState({displayedAssets: _displayedAssets});
    }

    renderList(assets) {
        return <div className="elements">
                 {assets.map(p => <CardAsset key={p.id} asset={p} />)}
                </div>;
    }

    renderPagination() {
        
        let amountOfPages = Math.ceil(this.props.assets.length / this.state.resultsPerPage);
        if (amountOfPages > 1) {
            let arr = [...Array(amountOfPages).keys()].map(x => ++x);
            let _pageIndex = this.state.pageIndex + 1;
            return (
                <div className="paginador">
                    {arr.map(el => {
                        if (el === _pageIndex) {
                            return <span className="active" onClick={() => this.reloadPaginator(el - 1)}>{el}</span>
                        }
                        return <span onClick={() => this.reloadPaginator(el - 1)}>{el}</span>                   
                    })}
                </div>
            );
        }
        
    }


    render() {
        if (this.props.assets) {
            return (
                <div className="elements-wrapper">
                    <h3>Assets</h3>
                    <FilterApplied />
                    {this.renderList(this.state.displayedAssets)}
                    {this.renderPagination()}
                </div>
            )
        }

        return (
            <div>
        </div>
        )

    }
}

const mapStateToProps = (state) => {
    return {
        assets: state.assets,
        filterPages: state.filterPages
    }
}

export default connect(mapStateToProps, actions)(Assets);
