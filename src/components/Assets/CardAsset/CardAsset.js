import React, { Component } from 'react'

class CardAsset extends Component {
    
  render() {
    return (
      <div className="element">
        <span className="book-mark">BM</span>
        <h4>{this.props.asset.description[0][6].name}</h4>
        <div>Audience: {this.props.asset.audience}</div>
        <div className="languages-tags">
        {this.props.asset.languages.map(p => <span className="languages-tag">{p}</span>)}
        </div>
        
        <div className="element-links" style={{textAlign:'center'}}>
          <a href='#'>More details</a>  |   <a href='#'>Download</a>
        </div>
      </div>
    )
  }
}
export default CardAsset;