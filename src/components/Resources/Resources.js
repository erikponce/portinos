import React, { Component } from 'react'
import { connect } from 'react-redux';
import * as actions from '../../actions/fetchActions';
import CardResources from './CardResources/CardResources';
import FilterApplied from '../FilterApplied/FilterApplied';

class Resources extends Component {

    constructor(props) {
        super(props);
        this.state = {
            // resource: this.props.resource,
            displayedResources: [],
            pageIndex: 0,
            resultsPerPage: 6
        }
    }


    componentDidMount() {
       // this.props.cleanFilterApplied();
        this.props.fetchResources();
    }

    componentWillReceiveProps(nextProps) {
        
        if (nextProps.filterPages && nextProps.resources){
            let rpp = parseInt(nextProps.filterPages);
            this.setState({resultsPerPage: rpp});
            this.loadPaginator(nextProps.resources, rpp);
        } else if (nextProps.resources) {
            this.loadPaginator(nextProps.resources, this.state.resultsPerPage);
        }
        
    }

    loadPaginator(_resources, _resultsPerPage) {
        if (_resultsPerPage <= _resources.length) {
            let _displayedResources = [..._resources].slice((this.state.pageIndex * _resultsPerPage), (this.state.pageIndex * _resultsPerPage) + _resultsPerPage);
            this.setState({displayedResources: _displayedResources});
        } else {
            this.setState({displayedResources: _resources});
        }
    }

    reloadPaginator(pIndex) {
        this.setState({pageIndex : pIndex})
        let _displayedResources = [...this.props.resources].slice((pIndex * this.state.resultsPerPage), (pIndex * this.state.resultsPerPage) + this.state.resultsPerPage);
        this.setState({displayedResources: _displayedResources});
    }

    renderList(resources) {
        return <div className="elements">
                 {resources.map(p => <CardResources key={p.id} resource={p} />)}
                </div>;
    }

    renderPagination() {
        
        let amountOfPages = Math.ceil(this.props.resources.length / this.state.resultsPerPage);
        if (amountOfPages > 1) {
            let arr = [...Array(amountOfPages).keys()].map(x => ++x);
            let _pageIndex = this.state.pageIndex + 1;
            return (
                <div className="paginador">
                    {arr.map(el => {
                        if (el === _pageIndex) {
                            return <span className="active" onClick={() => this.reloadPaginator(el - 1)}>{el}</span>
                        }
                        return <span onClick={() => this.reloadPaginator(el - 1)}>{el}</span>                   
                    })}
                </div>
            );
        }
        
    }


    render() {
        if (this.props.resources) {
            return (
                <div className="elements-wrapper">
                    <h3>Resources</h3>
                    <FilterApplied />
                    {this.renderList(this.state.displayedResources)}
                    {this.renderPagination()}
                </div>
            )
        }

        return (
            <div>
        </div>
        )

    }
}

const mapStateToProps = (state) => {
    return {
        resources: state.resources,
        filterPages: state.filterPages
    }
}

export default connect(mapStateToProps, actions)(Resources);
