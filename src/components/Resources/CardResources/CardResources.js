import React, { Component } from 'react'

class CardResources extends Component {
    
  render() {
      //console.log(this.props.asset);
    return (
      <div className="element">
        <h4>{this.props.resource.description[0][1].name}</h4>

        <div className="languages-tags">
          {this.props.resource.languages.map(p => <span className="languages-tag">{p}</span>)}
        </div>

        <div className="element-links" style={{textAlign:'center'}}>
          <a href='#'>More details</a>
        </div>
      </div>
    )
  }
}

export default CardResources;