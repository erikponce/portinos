import React, { Component } from 'react'

class CardCampaign extends Component {
    
  render() {
    return (
      <div className="element">
        <h4>{this.props.campaign.description[0][2].name}</h4>
        <div>Audience: {this.props.campaign.audience}</div>
        <div className="languages-tags">
        {this.props.campaign.languages.map(p => <span className="languages-tag">{p}</span>)}
        </div>

        <div className="element-links" style={{textAlign:'center'}}>
          <a href='#'>More details</a>  |   <a href='#'>Download</a>
        </div>
      </div>
    )
  }
}
export default CardCampaign;