import React, { Component } from 'react'
import { connect } from 'react-redux';
import * as actions from '../../actions/fetchActions';
import CardCampaign from './CardCampaign/CardCampaign';
import FilterApplied from '../../components/FilterApplied/FilterApplied';

class Campaigns extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            // campaign: this.props.campaign,
            displayedCampaigns: [],
            pageIndex: 0,
            resultsPerPage: 6
        }
    }


    componentDidMount() {
       // this.props.cleanFilterApplied();
        this.props.fetchCampaigns();
    }

    componentWillReceiveProps(nextProps) {
        
        if (nextProps.filterPages && nextProps.campaigns){
            let rpp = parseInt(nextProps.filterPages);
            this.setState({resultsPerPage: rpp});
            this.loadPaginator(nextProps.campaigns, rpp);
        } else if (nextProps.campaigns) {
            this.loadPaginator(nextProps.campaigns, this.state.resultsPerPage);
        }
        
    }

    loadPaginator(_campaigns, _resultsPerPage) {
        if (_resultsPerPage <= _campaigns.length) {
            let _displayedCampaigns = [..._campaigns].slice((this.state.pageIndex * _resultsPerPage), (this.state.pageIndex * _resultsPerPage) + _resultsPerPage);
            this.setState({displayedCampaigns: _displayedCampaigns});
        } else {
            this.setState({displayedCampaigns: _campaigns});
        }
    }

    reloadPaginator(pIndex) {
        this.setState({pageIndex : pIndex})
        let _displayedCampaigns = [...this.props.campaigns].slice((pIndex * this.state.resultsPerPage), (pIndex * this.state.resultsPerPage) + this.state.resultsPerPage);
        this.setState({displayedCampaigns: _displayedCampaigns});
    }

    renderList(campaigns) {
        return <div className="elements">
                 {campaigns.map(p => <CardCampaign key={p.id} campaign={p} />)}
                </div>;
    }

    renderPagination() {
        
        let amountOfPages = Math.ceil(this.props.campaigns.length / this.state.resultsPerPage);

        if (amountOfPages > 1) {
            let arr = [...Array(amountOfPages).keys()].map(x => ++x);
            let _pageIndex = this.state.pageIndex + 1;
            return (
                <div className="paginador">
                    {arr.map(el => {
                        if (el === _pageIndex) {
                            return <span className="active" onClick={() => this.reloadPaginator(el - 1)}>{el}</span>
                        }
                        return <span onClick={() => this.reloadPaginator(el - 1)}>{el}</span>                   
                    })}
                </div>
            );
        }
        
    }


    render() {
        if (this.props.campaigns) {
            return (
                <div className="elements-wrapper">
                    <h3>Campaigns</h3>
                    <FilterApplied />
                    {this.renderList(this.state.displayedCampaigns)}
                    {this.renderPagination()}
                </div>
            )
        }

        return (
            <div>
        </div>
        )

    }
}

const mapStateToProps = (state) => {
    return {
        campaigns: state.campaigns,
        filterPages: state.filterPages
    }
}

export default connect(mapStateToProps, actions)(Campaigns);