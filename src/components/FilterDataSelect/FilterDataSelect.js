import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions/fetchActions';

class FilterDataSelect extends Component {
    constructor(props){
        super(props);
        this.state={
           
        }
    }

    handleChange(value) {
        this.props.fetchFilterData(value);
      }
    
  render() {
    return (
     <div>
         Filtrar por
         <select onChange={(e)=>this.handleChange(e.target.value)}>
             <option value='1'>Relevancia</option>
             <option value='2'>Fecha Asc.</option>
             <option value='3'>Fecha Desc.</option>
         </select>
     </div>
    )
  }
}

export default connect(null, actions)(FilterDataSelect);