import React, { Component } from 'react'
import {connect} from 'react-redux';
import * as actions from '../../actions/fetchActions';
import getDataAuth from '../../utils/getDataAuth';
import { createBrowserHistory } from 'history';
const history = createBrowserHistory();


class Header extends Component {

    componentWillReceiveProps(nextProps) {
      if(!nextProps.auth){
        localStorage.removeItem('auth');
        localStorage.removeItem('preferences');
        window.location.href='/';
      }
    }

    logout(){
      this.props.fetchLogOut();
      //localStorage.removeItem('auth');
      //window.location.href='/';
    }

    login(){
      //this.props.history.push('/login');

      window.location.href='/login';
    }
    
  render() {
    return (
     <div className="header">
       {this.props.auth || getDataAuth() !== ''?
       <div>
       <input type='button' value='Logout' onClick={()=>this.logout()}></input></div>
        : <div>
        <input type='button' value='Login' onClick={()=>this.login()}></input>

        </div>}
     </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  auth: state.auth
  }
 }

export default connect(mapStateToProps,actions)(Header);
