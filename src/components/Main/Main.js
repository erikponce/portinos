import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Login from '../../containers/LoginPage/LoginPage';
import Home from '../../containers/HomePage/HomePage';
import ListAssetPage from '../../containers/ListAssetPage/ListAssetPage';
import ListCampaignPage from '../../containers/ListCampaignPage/ListCampaignPage';
import ListResourcePage from '../../containers/ListResourcePage/ListResourcePage';

class Main extends Component {
  render() {
    return (
      
          <Router>
            <div>
            <Switch>
              <Route exact path="/" component={Home} />
              <Route path="/login" component={Login} />
              <Route path="/listAsset" component={ListAssetPage} />
              <Route path="/listCampaign" component={ListCampaignPage} />
              <Route path="/listResources" component={ListResourcePage} />
            </Switch>
            </div>
          </Router>
      
    )
  }
}

export default Main;