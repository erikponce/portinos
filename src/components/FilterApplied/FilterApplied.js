import React, { Component } from 'react'
import {connect} from 'react-redux';
import * as actions from '../../actions/fetchActions';

class FilterApplied extends Component {

    componentWillReceiveProps(nextProps) {
     
    }

    renderFiltersApplied(filters) {
        return <div className="filter-buttons">
                 {filters.map(p => <span className="filters-applied-tag">{p.name}<span onClick={()=>this.props.removeFilter(p)}> X</span></span>)}
                </div>;
    }

    render() {
        
    return (
     <div>
         {this.renderFiltersApplied(this.props.filterApplied)}
     </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    filterApplied: state.filterApplied
  }
 }

export default connect(mapStateToProps,actions)(FilterApplied);