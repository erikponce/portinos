import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions/fetchActions';

class FilterPages extends Component {
    constructor(props){
        super(props);
        this.state={
        }
    }

    handleChange(value) {
        this.props.fetchFilterPages(value);
      }
    
  render() {
    return (
     <div>
         Cantidad de paginas
         <select value={this.state.value} onChange={(e)=>this.handleChange(e.target.value)}>
             <option value='6'>6</option>
             <option value='12' >12</option>
             <option value='18'>18</option>
         </select>
     </div>
    )
  }
}

export default connect(null, actions)(FilterPages);
