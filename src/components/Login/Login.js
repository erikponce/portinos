import React, { Component } from 'react'
import {connect} from 'react-redux';
import * as actions from '../../actions/fetchActions';

class Login extends Component {
    constructor(props){
        super(props);
        this.state={
        username:'',
        password:''
        }
    }

    componentWillReceiveProps(nextProps) {
        if((this.props.auth !== nextProps.auth) && nextProps.auth){
            this.props.fetchPreferencesUser();
        }
        if(nextProps.preferences){
            localStorage.setItem('preferences', JSON.stringify(nextProps.preferences));
            window.location.href='/';
        }
    }

    updateInputValue(evt) {

        if(evt.target.name === 'user'){
            this.setState({
             username: evt.target.value
            });
        }
        else{
            this.setState({
                password: evt.target.value
            });
        }
        
    }

    sendData(data){
        this.props.fetchAuthLogin(data.username,data.password);
    }

    render() {

    return (
     <div>
         User:
         <input type='text' name='user' onChange={evt => this.updateInputValue(evt)}></input><br />
         Password:
         <input type='password' name='pass' onChange={evt => this.updateInputValue(evt)}></input><br />
         <input type='button' value='Enviar' onClick={()=>this.sendData(this.state)}></input><br />
     </div>
    )
  }
}

const mapStateToProps = (state) => {
    return {
    auth: state.auth,
    preferences: state.preferences

    }
   }

export default connect(mapStateToProps,actions)(Login);
