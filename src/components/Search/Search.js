import React, { Component } from 'react'
import {connect} from 'react-redux';
import * as actions from '../../actions/fetchActions';
import FilterPages from '../../components/FilterPages/FilterPages'
import FilterDataSelect from '../../components/FilterDataSelect/FilterDataSelect'

class Search extends Component {
    constructor(props){
        super(props);
        this.state={
          searchString:'',
        }
    }

    updateInputValue(evt) {
      this.setState({
        searchString: evt.target.value
      });
    }

    search(searchString, name){
      this.props.cleanListAssets();
      console.log(searchString);
      this.props.fetchSearch(searchString, name);
    }
    
  render() {
    return (
     <div>
       <table style={{width:'60%'}}>
         <tbody>
          <tr>
            <td>
                <input type='text' name='searchString' onChange={evt => this.updateInputValue(evt)}/>
                <input type='button' value='Buscar' onClick={()=>this.search(this.state.searchString, 'campaigns')}></input>
              </td>
              <td>
                <FilterPages />
              </td>
              <td>
                <FilterDataSelect />
              </td>
            </tr>
          </tbody>
        </table>
     </div>
    )
  }
}
export default connect(null,actions)(Search);
